# gpo_ideal_ballooning_stability

A small example of using Gaussian Process Optimisation (GPO) for
identification of the ideal ballooning stability boundary of relevance
to tokamak plasmas.


## Getting started

This example has a small number of dependencies:

* python3 (minimum version?) and modules defined in requirements.txt : Provides the GPO framework, plotting etc.
* [GS2](https://bitbucket.org/gyrokinetics/gs2) : Provides a code to solve the ideal ballooning stability problem

To get up and running one might want to create a python virtual environment

~~~
python -m venv gpo
source gpo/bin/activate
pip install -r requirements.txt
~~~

Then clone GS2

~~~
git clone https://bitbucket.org/gyrokinetics/gs2.git
~~~

Follow the [setup instructions](https://gyrokinetics.gitlab.io/gs2/index.html)

~~~
cd gs2
make submodules
# Create Makefiles/<system_id>
export GK_SYSTEM=<system_id>
export MAKEFLAGS=-IMakefiles
make USE_MPI= ideal_ball
~~~

It is recommended to build `ideal_ball` without MPI in order to speed
up process initialisation.

When running the python _can_ use OpenMP and will by default use all
threads. This often leads to suboptimal performance and one may wish
to limit the number of threads, e.g.

~~~
OMP_NUM_THREADS=2 python gpo.py
~~~

Running with the example `template.in` should produce the following figure

![Filled contours showing GP values and stability boundaries from the GP in black and full model in white for zero theta0](./docs/example_theta0_zero.png)

where the black contour represents the stability boundary found from
the GP model, the white contour represents the "true" boundary from
the full model and red crosses show the evaluation points.

Changing `theta0` to `1.5707` gives

![Filled contours showing GP values and stability boundaries from the GP in black and full model in white for theta0 of pi/2](./docs/example_theta0_pi_by_two.png)
