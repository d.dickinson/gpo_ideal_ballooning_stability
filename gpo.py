import f90nml
from bayes_opt import BayesianOptimization
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
from bayes_opt import UtilityFunction
from numpy import loadtxt, vstack, sum
import subprocess

# Define a custom acquisition function. Here the idea is to prefer
# points where the predicted value is close to user determined target
# when average over three nearby points, where the distance between
# points is set by the step input. This is helpful to look for boundaries.
# The use of nearby points is not really required but can help a touch.
class boundary_acq(UtilityFunction):
    def __init__(self, target, step):
        self.count = 0
        self.target = target
        self.step = step
        self.use_nearby = step > 0

    def update_params(self):
        self.count += 1

    def utility(self, x, gp, y_max):
        return self._targ(x, gp, self.target, self.step, self.use_nearby)

    @staticmethod
    def _targ(x, gp, target, step, use_nearby):
        if use_nearby:
            tmp = vstack([x, x*(1-step), x*(1+step)])
            values = gp.predict(tmp).reshape([-1, x.shape[0]])
            values = sum(values, axis = 0)/tmp.shape[0]
        else:
            values = gp.predict(x)
        return -abs(target-values) #1.0/(0.1 + abs(target - values))

class function_wrapper:
    def __init__(self, input_name = 'template.in',
                 output_name = 'step.in', ideal_ball = './ideal_ball'):
        self.nml = f90nml.read(input_name)
        self.output_name = output_name
        self.result_name = self.output_name.split('.')[0]+'.ballstab_2d'
        self.ideal_ball = ideal_ball
        self.count = 0

    # Take current shear and beta' values are returns the stability flag
    # (0 = stable, > 0 = unstable). We limit the stability flag returned
    # to be <= 1 as we are not interested in finding where the flag is >1
    def __call__(self, shat, beta_prime):
        self.count += 1
        self.nml['theta_grid_eik_knobs']['s_hat_input'] = shat
        self.nml['theta_grid_eik_knobs']['beta_prime_input'] = beta_prime
        self.nml.write(self.output_name, force = True)
        subprocess.run([self.ideal_ball, self.output_name])
        return min(loadtxt(self.result_name), 1.0)

if __name__ == "__main__":
    import time
    start = time.time()
    bounds = {'shat' : (0.05, 5.0), 'beta_prime' : (-1.5, -0.01)}
    func = function_wrapper()
    # Create the GPO instance
    optimizer = BayesianOptimization(
        f = func,
        pbounds = bounds,
        verbose = 2,
        random_state = 1,
        )

    # Create the acquisition function. As the stability boundary
    # we're looking for is at the transition from 0 to 1 we pick
    # a target of 0.5
    utility = boundary_acq(target = 0.5, step = 0.001)

    # Try to maximize the stability flag - note we don't actually
    # want to maxmize the flag, rather find the transition from 0 to 1
    optimizer.maximize(
        init_points = 5,
        n_iter = 60,
        acquisition_function = utility,
    )
    mid = time.time()
    print("Done with optimisation in "+str(mid-start))

    # Now we move on to visualising the results. First we extract the sample
    # points and target functions for all points encountered and fit the GP
    # instance to this.
    xobs = [[x['params']['shat'], x['params']['beta_prime']] for x in optimizer.res]
    yobs = [x['target'] for x in optimizer.res]
    optimizer._gp.fit(xobs, yobs)

    # Now we create a cartesian grid of the inputs on which we want to
    # visualise the current GP and then evaluate GP on each point
    from numpy import linspace, zeros
    nshat = 101 ; nbeta_prime = 151
    shat = linspace(bounds['shat'][0], bounds['shat'][1], nshat)
    bp = linspace(bounds['beta_prime'][0], bounds['beta_prime'][1], nbeta_prime)
    gp_prediction = zeros([nshat, nbeta_prime])
    samples = zeros([nshat * nbeta_prime, 2])
    counter = 0
    for i, sh in enumerate(shat):
        for j, b in enumerate(bp):
            samples[counter, : ] = [sh, b]
            counter += 1
    gp_prediction = optimizer._gp.predict(samples).reshape(gp_prediction.shape)

    mid2 = time.time()
    print("Done with evaluating GP in "+str(mid2-mid))

    # Now let us run the ideal model on the same cartesian grid for comparison
    nml = f90nml.read("template.in")
    nml['theta_grid_eik_knobs']['s_hat_input'] = 0.0
    nml['theta_grid_eik_knobs']['beta_prime_input'] = -1.0
    nml['ballstab_knobs']['n_beta'] = nbeta_prime
    nml['ballstab_knobs']['n_shat'] = nshat
    nml['ballstab_knobs']['shat_min'] = shat[0]
    nml['ballstab_knobs']['shat_max'] = shat[-1]
    nml['ballstab_knobs']['beta_mul'] = abs(bp).max()
    nml['ballstab_knobs']['beta_div'] = 1.0/abs(bp).min()
    nml.write("scan.in", force = True)
    subprocess.run(["./ideal_ball", "scan.in"])
    stability = loadtxt('scan.ballstab_2d')
    ss = loadtxt("./scan.ballstab_shat") ; bb = loadtxt("./scan.ballstab_dbdr")

    mid3 = time.time()
    print("Done with evaluating real model in "+str(mid3-mid2))

    # Now plot!
    import matplotlib.pyplot as plt
    plt.figure()
    plt.pcolormesh(-bp, shat, gp_prediction, shading = 'auto')
    plt.colorbar()
    plt.contour(-bp, shat, gp_prediction, levels=[0.5], colors=['black'])
    plt.contour(-bb, ss, stability.T, levels=[0.5], colors=['white'])
    for i, points in enumerate(optimizer.res):
        plt.plot(-points['params']['beta_prime'], points['params']['shat'], 'x', color='red')
    plt.xlabel('|Beta prime|') ; plt.ylabel('Shat')
    end = time.time()
    print("Plotting took "+str(end - mid3))
    print("Finished in "+str(end-start))
    plt.show()
